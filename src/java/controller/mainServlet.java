
package controller;

import dataaccess.databaseUtil;
import dataaccess.userDatabase;
import getAndSet.Events;
import security.securityUtil;
import getAndSet.Message;
import getAndSet.Feedbacks;
import getAndSet.Project;
import getAndSet.User;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.JOptionPane;



@WebServlet(name = "mainServlet", urlPatterns = {"/mainServlet"})
@MultipartConfig
public class mainServlet extends HttpServlet {
    
    @Override
@SuppressWarnings("null")
    protected void doGet(HttpServletRequest request,HttpServletResponse response)
            throws ServletException, IOException, FileNotFoundException {
        
        
        String action = request.getParameter("action");
    
        String url = "/home.jsp";
        
        User user = new User();
         
        
        ServletContext servletContext = request.getSession().getServletContext();
        
        HttpSession session = request.getSession();
        
 
        User user4 = (User) session.getAttribute("user");
        
        Events event = new Events();
        
        
        
        
////////////////////////////////////////////////////////////////////////////////

//Registration
        if(action.equals("register")){
        
        //Registration Parameters
        String email  = request.getParameter("email");
        String password  = request.getParameter("password");
        String fullName = request.getParameter("fullName");
        String username = request.getParameter("username");
        String instructor = request.getParameter("instructor");
        String group = request.getParameter("group");
        String userID = user.createUserId();
        String securePassword=null;
        user.setSalt(securityUtil.getSalt());
        
        
                    try {
       securePassword=securityUtil.hashAndSaltPassword(password, user.getSalt());
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(mainServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        
        
                //store variables in User object and insert into database
         user.setFullname(fullName);
         user.setEmail(email);
         user.setPassword(securePassword);
         user.setUsername(username);
         user.setInstructor(instructor);
         user.setGroup(group);
         user.setUserID(userID);
         user.setTaskCount(0);
         user.setCompletedTaskCount(0);
         
         


         
         
            try {
                userDatabase.insert(user);
            } catch (SQLException ex) {
                Logger.getLogger(mainServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        
        url="/login.jsp";
         }
////////////////////////////////////////////////////////////////////////////////        
        
 //Login Validation      
 if(action.equals("login")){
                 
     
     String username  = request.getParameter("username");
     String password  = request.getParameter("password");
              
     
     
   
     
     
     
    try {
      
      user4 = userDatabase.select(username); 
      
      
      
            try {
                userDatabase.getTaskCount(user4.getUserID());
                  userDatabase.getCompletedTaskCount(user4.getUserID());
                  userDatabase.getAllTaskCount(user4);
                  userDatabase.getAllCompletedTaskCount(user4);
            } catch (SQLException ex) {
                Logger.getLogger(mainServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
                                  
                      
      password = securityUtil.hashAndSaltPassword(password, user4.getSalt());
      
      
          if(user4.getInstructor().equals("yes")){
            ArrayList<User> userList = userDatabase.selectAllUsers();
                session.setAttribute("userList", userList);
            }else{
              ArrayList<User> userList = userDatabase.selectGroupUsers(user4);
                session.setAttribute("userList", userList);
          }
      

        if(user4.getUsername().equals(null)){
                    
            url="/signup.jsp";
            String message="That username isn't registered yet! ";
            //user.setMessage(message);
        
        }else if (username.equals(user4.getUsername())&& password.equals(user4.getPassword())){
            
            
            session.setAttribute("login",true);
            session.setAttribute("user",user4);
           
      
            url="/home.jsp";
        }
        
        
        else{
           String message="User or password incorrect. ";
            //user.setMessage(message);
            
            url="/login.jsp";
            
        }
        
        
 
    } catch (SQLException ex) {
                Logger.getLogger(mainServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(mainServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
    
    
    

}




        
        
 ///////////////////////////////////////////////////////////////////////////////       
        if(action.equals("logout")){// used to log out 
            
            
    session.invalidate();
    session = request.getSession(); // so if no session is active no session is created
   
    
    if (session != null)
        session.setMaxInactiveInterval(1); 
    
    //Deletes Cookies
    Cookie[] cookies = request.getCookies();
        for (Cookie cookie : cookies) {
        cookie.setMaxAge(0); //delete the cookie
        cookie.setPath("/"); //allow the download application to access it
        response.addCookie(cookie);
} 
    
    
    url="/login.jsp";
    }
        
        
 ///////////////////////////////////////////////////////////////////////////////       
   Message message = new Message();
 
 if(action.equals("message")||action.equals("home")){//writes tweets to the tweet txt file and reloads page

            String text = request.getParameter("userInput");
            User user3 = (User) session.getAttribute("user");
            
            
           
           ArrayList<Message> usersMessages = userDatabase.getMessages(user3);
                       


           
            if(text!=null || text!=""){

                message.setText(text);
                message.setMessageID(message.createMessageId());
                message.setUserId(user3.getUserID());
                message.setDate(message.getCurrentDate());
                message.setFullname(user3.getFullname());
                message.setUsername(user3.getUsername());
                message.setGroupNumber(user3.getGroup());
                
                
                user.setFullname(user3.getFullname());
                user.setUsername(user3.getUsername());
                 
                try {
                    userDatabase.insertMessages(message);
                } catch (SQLException e) {
             
                }
                
                 
                session.setAttribute("user",user3);
                session.setAttribute("usersMessage", usersMessages);
              
              
            }
            url="/home.jsp";
           
   }
 
 Feedbacks feedbacks = new Feedbacks();
 
 if(action.equals("feedbacks")){//writes tweets to the tweet txt file and reloads page

            String text = request.getParameter("userInput");
            User user6 = (User) session.getAttribute("user");
            

           ArrayList<Feedbacks> usersFeedback = userDatabase.getFeedbacks(user6);
                       

            if(text!=null || text!=""){

                feedbacks.setMessageText(text);
                feedbacks.setFeedbackID(feedbacks.createFeedbackId());
                feedbacks.setUserID(user6.getUserID());
                
                
                 
                try {
                    userDatabase.insertMessages(message);
                } catch (SQLException e) {
             
                }
                
                 
                session.setAttribute("user",user6);
                session.setAttribute("usersFeedback", usersFeedback);
              
              
            }
            url="/feedback.jsp";
           
   }
 
 
 
 
 if(action.equals("update")){
    User currentUser = (User) session.getAttribute("user");
     
        User userInfo=null;
    try {
           userInfo=userDatabase.select(currentUser.getUsername());
        } catch (SQLException ex) {
            
        }

    currentUser.setFullname(userInfo.getFullname());
    currentUser.setPassword(userInfo.getPassword());
    currentUser.setSalt(userInfo.getSalt());
    
    String fullName = request.getParameter("fullName");
    String email = request.getParameter("email");
    String password = request.getParameter("password");
    

    
    

    if( fullName!=null || !fullName.equals("")){
        currentUser.setFullname(fullName);
    }
    
    
      if( email!=null || !email.equals("")){
        currentUser.setEmail(email);
        }

   
    if(password!=null || password!=""){
    if(!(currentUser.getPassword().equals(password))){
        
        
         String salt = securityUtil.getSalt();
      
         
      String hashedPassword=null;
            try {
                hashedPassword = securityUtil.hashAndSaltPassword(password,salt);
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(mainServlet.class.getName()).log(Level.SEVERE, null, ex);
            } 
    currentUser.setSalt(salt);
    currentUser.setPassword(hashedPassword);
    }
    }else{
        
    System.out.println("NO password entered! ");
        
    }
  
    int i=userDatabase.updateUser(currentUser);
    
    if(i==0){
        System.out.println("update successful");
        
        
        Cookie[] cookies = request.getCookies();
        for (Cookie cookie : cookies) {
        cookie.setMaxAge(0); //delete the cookie
        cookie.setPath("/"); //allow the download application to access it
        response.addCookie(cookie);
} 
        

            
    }else{
        System.out.println("Executed successfully "+i+" rows affected.");
    }
    
    session.setAttribute("user",currentUser);
    url="/home.jsp";
}
 
 
 
 
 
 if(action.equals("tasks")){
     
     Project project = new Project();
          
  
               ArrayList<Project> usersTasks = userDatabase.getTasks(project);
   
               
               
    String task = request.getParameter("task");
    String assignedTo = request.getParameter("assigned");
    String dueDate = request.getParameter("dueDate");
    String status = request.getParameter("status");
    String feedback = request.getParameter("feedback");
    String priority = request.getParameter("priority");
    


    
    
    if(task!=null&&task!="")
    
                project.setTaskID(user4.createUserId());
                project.setTask(task);
                project.setAssignTo(assignedTo);
                project.setDueDate(project.convertDate(dueDate));
                project.setPriority(priority);
 
    
                
              try {
                
                  userDatabase.getCompletedTaskCount(user4.getUserID());
                  userDatabase.getAllTaskCount(user4);
                  userDatabase.getAllCompletedTaskCount(user4);
            } catch (SQLException ex) {
                Logger.getLogger(mainServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
                                  
              
              
            if(!assignedTo.equals(user4.getUserID())){
         try {
             userDatabase.getAssignTaskCount(assignedTo);
         } catch (SQLException ex) {
             Logger.getLogger(mainServlet.class.getName()).log(Level.SEVERE, null, ex);
         }
         
                user4.addAssignedTaskCount(1);
                userDatabase.updateAssignTaskCount(project, user4);
            }else{
         try {
             userDatabase.getTaskCount(user4.getUserID());
         } catch (SQLException ex) {
             Logger.getLogger(mainServlet.class.getName()).log(Level.SEVERE, null, ex);
         }
           
                 user4.addTaskCount(1);
                 userDatabase.updateTaskCount(project,user4);
            }
                
                      
                      
                       
                       
                       
                       
                       
    
            try {
                    userDatabase.insertTasks(project);
                } catch (SQLException e) {
             
                }
            
            
            
            
            
   
                       
 
             session.setAttribute("user",user4);
             session.setAttribute("usersTasks", usersTasks);
            
            
                   
        url="/projects.jsp";
 }
 
 ///////////////////////////////////////////////////////////////////////////////
 
  if(action.equals("feedback")){
     
     //JOptionPane.showMessageDialog(null, "In tasks");
     Project project = new Project();
     
               //User user4 = (User) session.getAttribute("user");

    ArrayList<Project> usersTasks = userDatabase.getTasks(project);
               
               
    String taskID = request.getParameter("taskID");
    String feedback2 = request.getParameter("feedback");
    
    
    if(taskID!=null&&taskID!="")
    
                project.setTaskID(taskID);
                project.setFeedback(feedback2);
 
  
                int i=userDatabase.addFeedback(project);   
                
               // JOptionPane.showMessageDialog(null,i);
                
            
             session.setAttribute("user",user4);
             session.setAttribute("usersTasks", usersTasks);
            
            
                   
        url="/projects.jsp";
 }
     
 
 
  ///////////////////////////////////////////////////////////////////////////////
 
  if(action.equals("updateStatus")){

     
      
     Project project = new Project();
     
     ArrayList<Project> usersTasks = userDatabase.getTasks(project);
                    
           
    String taskID = request.getParameter("taskID");
    String status = request.getParameter("status");
     String assignedTo = request.getParameter("assignedTo");
    

    int i=0;

               
                project.setTaskID(taskID);
                project.setStatus(status);
                project.setAssignTo(assignedTo);
                
                           try {
                userDatabase.getTaskCount(user4.getUserID());
                  userDatabase.getCompletedTaskCount(user4.getUserID());
                  userDatabase.getAllTaskCount(user4);
                  userDatabase.getAllCompletedTaskCount(user4);
            } catch (SQLException ex) {
                Logger.getLogger(mainServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
                                  
            
                        
             if(status.equals("Completed")){
                 user4.addCompletedTasksCount(1);
        
                    userDatabase.updateCompletedTaskCount(user4, project);
              }
             
             
             
                 userDatabase.updateStatus(project);  
             

                 
             session.setAttribute("user",user4);
             session.setAttribute("usersTasks", usersTasks);
                   
        url="/projects.jsp";
 }
     
 

 ///////////////////////////////////////////////////////////////////////////////  
 if(action.equals("events")){
     
          
     //Events event = new Events();
     //User user5 = (User) session.getAttribute("user");
    ArrayList<Events> usersEvents = userDatabase.getEvents(event);
              
    String eventName = request.getParameter("eventName");
    String date = request.getParameter("eventDate");
    String eventMsg = request.getParameter("eventMsg");
    
    
    if(eventName!=null&&eventName!="")
    
                event.setEventID(user4.createUserId());
                event.setEventName(eventName);
                event.setEventDate(event.convertDate(date));
                event.setEventMsg(eventMsg);
                
                
                
            try {
                    userDatabase.insertEvents(event);
                } catch (SQLException e) {
             
                }    
                
            
             session.setAttribute("user",user4);
             session.setAttribute("usersEvents", usersEvents);
            
     
     url="/calendar.jsp";
 }
 
/////////////////////////////////////////////////////////////////////////////// 

 if(action.equals("delete")){
    
     
     String ID = request.getParameter("messageID");
     int messageID = Integer.parseInt(ID);
     
      
     
     int i=userDatabase.delete(messageID);
     if(i==1){
         System.out.print("delete message successful\n");
     }
     
     
    
     url="/home.jsp";
 }
 
 
        try{
                Project project = new Project();
     
               ArrayList<Project> usersTasks = userDatabase.getTasks(project);
               
                ArrayList<Events> usersEvents = userDatabase.getEvents(event);

           ArrayList<Message> showUserMessages = userDatabase.getMessages(user4);
           
           
                                 try {
                userDatabase.getTaskCount(user4.getUserID());
                  userDatabase.getCompletedTaskCount(user4.getUserID());
                  userDatabase.getAllTaskCount(user4);
                  userDatabase.getAllCompletedTaskCount(user4);
            } catch (SQLException ex) {
                Logger.getLogger(mainServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
           
           
           
                       session.setAttribute("user",user4);
                session.setAttribute("usersMessage", showUserMessages);
                request.setAttribute("usersMessage",showUserMessages);
                session.setAttribute("usersEvents", usersEvents);
                session.setAttribute("usersTasks", usersTasks);
                request.setAttribute("usersTasks", usersTasks);
        }catch(Exception e){
                
                }
        
        
        
        // store User object in request
      getServletContext().getRequestDispatcher(url).forward(request, response);
        
        
        
    }//end of do get
    
    
    
    
    
    
    
    
    
    
    
      @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException, FileNotFoundException {
        doGet(request, response);
        

        
        
                }//end of do post 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}//end of main servlet
    
    
    
  
